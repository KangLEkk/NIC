import os
import torch
import cv2
import numpy as np

def load_model(model, resume):
    pretrained_dict = torch.load(resume)
    model_dict = model.state_dict()
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
    #for k in pretrained_dict:
    #    print(k)
    model_dict.update(pretrained_dict)
    model.load_state_dict(model_dict)
def load_img(img_path):
    img=cv2.imread(img_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = np.array(img, dtype=np.float32) / 255.0
    img = np.transpose(img, axes=(2, 0, 1))
    return img