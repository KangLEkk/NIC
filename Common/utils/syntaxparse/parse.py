from pathlib import Path
from Common.utils.syntaxparse.picture.bee_picture import BEE_picture
from Common.utils.syntaxparse.picture.nic_picture import NIC_picture
from Common.utils.syntaxparse.picture import iWave_picture
from Common.utils.syntaxparse.utils import read_uchars, write_uchars


class header():
    def __init__(self):
        self.coding_mode_selection_syntax = codingModeSelection_header()
        self.picture = None
    
    def construct_picture(self):
        if self.coding_mode_selection_syntax.bin2symbolIdx == 0:
            self.picture = BEE_picture() 
        if self.coding_mode_selection_syntax.bin2symbolIdx == 1:
            self.picture = iWave_picture.iWave_picture()
        if self.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.picture = NIC_picture()
    
    def read_json_cfg(self, cfgfile, image, rate):
        import json
        with open(cfgfile) as json_file:
            data = json.load(json_file)
            codingModelSelection_syntax = data["coding_model_seclection_syntax"]
            picture_syntax = data["picture_syntax"]
        self.coding_mode_selection_syntax.update(codingModelSelection_syntax)
        self.construct_picture()
        if self.coding_mode_selection_syntax.bin2symbolIdx == 1:
            self.picture.update(picture_syntax, image)
        else:
            self.picture.update(picture_syntax, image, rate)

    def write_header_to_stream(self, binName):
            f = Path(binName).open("wb")
            f = self.coding_mode_selection_syntax.write_header_to_stream(f)
            self.picture.write_header_to_stream(f)
            f.close()
            return

    def read_header_from_stream(self, f):
        f = self.coding_mode_selection_syntax.read_header_from_stream(f)
        self.construct_picture()
        self.picture.read_header_from_stream(f)

class codingModeSelection_header():
    def __init__(self):
        self.bin2symbolIdx = None
        self.arithmeticEngineIdx = None
        self.synthesisTransformIdx = None
        self.reserved_2_bits = 0
    
    def write_header_to_stream(self, f):
        code = self.bin2symbolIdx<<6 | self.arithmeticEngineIdx<<4 | self.synthesisTransformIdx<<2 | self.reserved_2_bits
        write_uchars(f, tuple([code]))
        return f

    def read_header_from_stream(self, f):
        code = read_uchars(f,1)[0]
        self.bin2symbolIdx = int((code & 0xC0)>>6)
        self.arithmeticEngineIdx = int((code & 0x30)>>4)
        self.synthesisTransformIdx = int((code & 0x0C)>>2)
        self.reserved_2_bits = code & 0x03
        return f
    
    def update(self, syntax):
        self.bin2symbolIdx = syntax["bin2symbolIdx"]
        self.arithmeticEngineIdx = syntax["arithmeticEngineIdx"]
        self.synthesisTransformIdx = syntax["synthesisTransformIdx"]
