import numpy as np
import torch
import torch.nn as nn

from Common.models.NIC_models.basic_module import P_NN
from Common.models.NIC_models.context_model import P_Model, Weighted_Gaussian_res_2mask, Weighted_Gaussian, Multistage
from Common.models.NIC_models.factorized_entropy_model import Entropy_bottleneck
from Common.models.NIC_models.hyper_module import h_synthesisTransform
from Common.models.NIC_models.model import Hyper_Dec, Dec


class NIC_Symbol2Bin(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        model_index = header.model_index
        if header.USE_VR_MODEL:
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
        else:
            M, N2 = 192, 128
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192

        if header.USE_MULTI_HYPER:
            if header.USE_PREDICTOR:
                # self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(128)
                if M == 192:
                    self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                    self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                elif M == 256:
                    self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                    self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                self.p_2 = P_Model(256)
                self.Y_2 = P_NN(256, 2)
            else:
                # self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(128)
                if M == 192:
                    self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                    self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                elif M == 256:
                    self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                    self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                self.p_2 = P_Model(256)
        else:
            # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
            self.factorized_entropy_func = Entropy_bottleneck(N2)

        self.p = P_Model(M)

        if header.USE_PREDICTOR:
            self.Y = P_NN(M, 2)
        else:
            self.hyper_dec = Hyper_Dec(N2, M)

        if header.USE_PREDICTOR:
            self.context = Multistage(M)
        else:
            self.context = Weighted_Gaussian(M)

        input_features = 3
        N1 = M
        M1 = M // 2
        self.decoder = Dec(input_features, N1, M, M1)

    @torch.no_grad()
    def encode(self, forward_trans_varlist, out_img, picture, device):
        header, block_header = picture.picture_header, picture.block_header
        # params_prob = hyper_dec
        i_rot = header.i_rot
        if header.USE_MULTI_HYPER:
            y_main, y_hyper, y_hyper_2 = forward_trans_varlist
            # Hyper 2
            y_hyper_2_q, xp3 = self.factorized_entropy_func(y_hyper_2, 2)

            # Hyper 1
            tmp2 = self.hyper_2_dec(y_hyper_2_q)
            hyper_2_dec = self.p_2(tmp2)
            if header.USE_PREDICTOR:
                y_hyper_predict = self.Y_2(tmp2)
                y_hyper_res = y_hyper - y_hyper_predict
                y_hyper_q = torch.round(y_hyper_res)
            else:
                y_hyper_q = torch.round(y_hyper)

            # Main
            if header.USE_PREDICTOR:
                tmp = self.hyper_1_dec(y_hyper_q + y_hyper_predict)
            else:
                tmp = self.hyper_1_dec(y_hyper_q)
            hyper_dec = self.p(tmp)
            if header.USE_PREDICTOR:
                y_main_predict = self.Y(tmp)
                y_main_res = y_main - y_main_predict
                y_main_q = torch.round(y_main_res)
            else:
                y_main_q = torch.round(y_main)

            y_main_q = torch.Tensor(y_main_q.cpu().numpy().astype(np.int))
            y_hyper_2_q = torch.Tensor(y_hyper_2_q.cpu().numpy().astype(np.int))
            y_hyper_q = torch.Tensor(y_hyper_q.cpu().numpy().astype(np.int))

            if header.GPU:
                y_main_q = y_main_q.cuda()
        else:
            y_main, y_hyper = forward_trans_varlist
            y_main_q = torch.round(y_main)
            y_main_q = torch.Tensor(y_main_q.cpu().numpy().astype(np.int))
            if header.GPU:
                y_main_q = y_main_q.cuda()

            # y_hyper_q = torch.round(y_hyper)

            y_hyper_q, xp2 = self.factorized_entropy_func(y_hyper, 2)
            y_hyper_q = torch.Tensor(y_hyper_q.cpu().numpy().astype(np.int))
            if header.GPU:
                y_hyper_q = y_hyper_q.cuda()
            y_hyper_2_q = None
            hyper_2_dec = None

        if header.USE_PREDICTOR:
            xp3, params_prob = self.context.compress(y_main_q, y_main_q + y_main_predict, hyper_dec,y_main_predict)
        else:
            xp3, params_prob = self.context(y_main_q, hyper_dec)
        #####################if use postprocessing, needs reconstruct the image from encoder side
        if header.USE_POSTPROCESSING:
            if header.USE_PREDICTOR:
                rec_img_block = self.decoder(y_main_q + y_main_predict, header.lambda_rd)
            else:
                rec_img_block = self.decoder(y_main_q, header.lambda_rd)
            if header.USE_GEO:
                if header.geo_index < 4:
                    rec_img_block = torch.rot90(rec_img_block, k=4 - i_rot, dims=[2, 3])
                else:
                    rec_img_block = torch.flip(torch.rot90(rec_img_block, k=4 - i_rot, dims=[2, 3]), dims=[2])
            output_ = torch.clamp(rec_img_block, min=0., max=1.0)
            out = output_.data[0].cpu().numpy()
            out = out.transpose(1, 2, 0)

            out_img[block_header.H_offset: block_header.H_offset + block_header.block_H,
            block_header.W_offset: block_header.W_offset + block_header.block_W, :] = out[:block_header.block_H, :block_header.block_W, :]

            block_header.W_offset += block_header.block_W
            if block_header.W_offset >= header.W:
                block_header.W_offset = 0
                block_header.H_offset += block_header.block_H

        return y_main_q, y_hyper_q, params_prob, y_hyper_2_q, hyper_2_dec, out_img
