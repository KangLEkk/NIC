import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import einops
import numpy as np
import AE
import os
from Common.models.NIC_models.basic_module import P_NN
from Common.models.NIC_models.context_model import P_Model, Weighted_Gaussian_res_2mask, Weighted_Gaussian, Multistage
from Common.models.NIC_models.hyper_module import h_synthesisTransform
from Common.models.NIC_models.model import Hyper_Dec


class NIC_Bin2Symbol(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        H, W, CTU_size, model_index = header.H, header.W, header.CTU_size, header.model_index
        if header.USE_VR_MODEL:
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
        else:
            M, N2 = 192, 128
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192

        if header.USE_MULTI_HYPER:
            if M == 192:
                self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
            elif M == 256:
                self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])

        self.p = P_Model(M)

        if header.USE_PREDICTOR:
            self.Y = P_NN(M, 2)
        else:
            self.hyper_dec = Hyper_Dec(N2, M)

        if header.USE_PREDICTOR:
            self.context = Multistage(M)
        else:
            self.context = Weighted_Gaussian(M)

    def Models2device(self,device):
        raise NotImplementedError

    def decode(self, y_hyper_q, header, block_header, bin_dir_path):
        if header.GPU:
            y_hyper_q = y_hyper_q.cuda()
            if header.USE_MULTI_HYPER:
                tmp = self.hyper_1_dec(y_hyper_q)
                hyper_dec = self.p(tmp)
                if header.USE_PREDICTOR:
                    y_main_predict = self.Y(tmp)
            else:
                hyper_dec = self.p(self.hyper_dec(y_hyper_q))
                print("check3")

            # precise = 16
            # enc_height, enc_width = block_header.enc_height, block_header.enc_width
            Min_Main, Max_Main = block_header.Min_Main, block_header.Max_Main

            # c_main = block_header.c_main
            # EC_this_block = block_header.EC_this_block
            #
            # h, w = int(enc_height / 16), int(enc_width / 16)
            # sample = np.arange(Min_Main, Max_Main + 1 + 1)  # [Min_V - 0.5 , Max_V + 0.5]
            #
            # sample = torch.FloatTensor(sample)
            # if header.GPU:
            #     sample = sample.cuda()

            # p3d = (5, 5, 5, 5, 5, 5)
            # y_main_q = torch.zeros(1, 1, c_main + 10, h + 10, w + 10)  # 8000x4000 -> 500*250
            ## prediction with padding
            # y_main_predict_padding = torch.zeros_like(y_main_q)
            # y_main_predict_padding[:, :, 5: -5, 5: -5, 5: -5] = y_main_predict.unsqueeze(1)

            # if header.GPU:
            #     y_main_q = y_main_q.cuda()
            #     y_main_predict_padding = y_main_predict_padding.cuda()
            if header.USE_VR_MODEL:
                header.lambda_rd = header.lambda_rd.cuda()
            AE.init_decoder(f"{bin_dir_path}/main.bin", Min_Main, Max_Main)
            # hyper = torch.unsqueeze(self.context.conv3(hyper_dec), dim=1)
            quant_latent = self.context.decompress(header=block_header, params=hyper_dec, predicted_y_hat=y_main_predict)

        ## added by sxd
        os.remove(f"{bin_dir_path}/main.bin")
        os.remove(f"{bin_dir_path}/hyper_1.bin")
        os.remove(f"{bin_dir_path}/hyper_2.bin")
        return quant_latent 
