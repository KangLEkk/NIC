import numpy as np
import torch
import torch.nn as nn

from Common.models.NIC_models.model import Enc


class NIC_ForwardTrans(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        model_index = header.model_index
        if header.USE_VR_MODEL:
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
        else:
            M, N2 = 192, 128
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192
        input_features = 3
        N1 = M
        M1 = M // 2
        self.encoder = Enc(input_features, N1, N2, M, M1)

    @torch.no_grad()
    def encode(self, im_block, header):
        if header.USE_MULTI_HYPER:
            y_main, y_hyper, y_hyper_2 = self.encoder(im_block, header.lambda_rd)
            return y_main, y_hyper, y_hyper_2
        else:
            y_main, y_hyper = self.encoder(im_block, header.lambda_rd)
            return y_main, y_hyper
