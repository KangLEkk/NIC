import torch.nn as nn

class BaseBin2Bits(nn.Module):
    def __init__(self):
        super().__init__()
        
    def decode(self, bits2bin_varlist, header, binName):
        # return quantized latent y_hat (example: y_hat)
        raise NotImplementedError
        