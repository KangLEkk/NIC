from Common.utils.bee_utils.csc import YCbCr2RGB
from Common.utils.bee_utils.tensorops import crop,resizeTensorFast,resizeTensor
import cv2
import numpy as np
import torch.nn as nn
import torch
from Common.HighLevel_Function.Postprocess.RestormerPostprocessing import Restormer




class BEE_Postprocess(nn.Module):
    def __init__(self):
        super().__init__()

    def YUV2RGB(self, imArray): #-》 imArrayRGB
        imArrayRGB = YCbCr2RGB()(imArray).clip(0,1)
        return imArrayRGB

    def ImageCropping(self, imArray, resized_size):
        imArray = crop(imArray, resized_size)
        return imArray

    def AdaptiveResampling(self, imArray, fast_resize_flag, original_size):
        if fast_resize_flag:
            imArray = resizeTensorFast(imArray, original_size)
        else:
            imArray = resizeTensor(imArray, original_size)
        return imArray
    
    def decode(self, imArray, header, recon_path):
        imArray = self.ImageCropping(imArray, [header.picture.picture_header.resized_size_h, 
                                               header.picture.picture_header.resized_size_w])
        imArray = self.AdaptiveResampling(imArray, header.picture.picture_header.fast_resize_flag, 
                                            [header.picture.picture_header.original_size_h, 
                                            header.picture.picture_header.original_size_w])
        imArray = self.YUV2RGB(imArray)
        if header.picture.picture_header.postprocessing == True:
            restormer = Restormer().cuda()
            #load the model
            #restormer.load_state_dict(torch.load('restormer.pth'))
            POST_CTU_SIZE = 512
            _, _, H, W = imArray.shape
            for h in range(0, H, POST_CTU_SIZE):
                for w in range(0, W, POST_CTU_SIZE):
                    block = torch.zeros((1, 3, POST_CTU_SIZE, POST_CTU_SIZE)).cuda()
                    block[:, :, :min(POST_CTU_SIZE, H-h), :min(POST_CTU_SIZE, W-w)] = imArray[:, :, h:min(H, h+POST_CTU_SIZE), w:min(W, w+POST_CTU_SIZE)]
                    with torch.no_grad():
                        block = restormer(block)
                    imArray[:, :, h:min(H, h+POST_CTU_SIZE), w:min(W, w+POST_CTU_SIZE)] = block[:, :, :min(POST_CTU_SIZE, H-h), :min(POST_CTU_SIZE, W-w)]

        self.writePngOutput(imArray, header.picture.picture_header.output_bit_depth, header.picture.picture_header.output_bit_shift, recon_path)
        return 0
        
    def writePngOutput(self, pytorchIm, bitDepth, bitshift, outFileName):
        output = outFileName
        if output is not None:
            img = pytorchIm.squeeze().permute(1, 2, 0).cpu().numpy()
            img = (img*((1<<bitDepth) - 1)).round()
            img = img.clip(0, (1<<bitDepth)-1)
            img *=  (1<<bitshift)
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            if bitDepth <= 8:
                cv2.imwrite(output, img.astype(np.uint8))
            else:
                cv2.imwrite(output, img.astype(np.uint16))
        else:
            print("not writing output.")
    
