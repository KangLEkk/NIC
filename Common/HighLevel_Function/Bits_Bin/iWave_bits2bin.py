import torch
import torch.nn as nn

import Common.models.iWave_models.Model as Model

class iWave_Bits2Bin(nn.Module):
    def __init__(self, header_in, **kwargs):
        super().__init__()
        header = header_in.picture.picture_header

        self.Transform_lossless = Model.Transform_lossless()
        self.CodingLL_lossless = Model.CodingLL_lossless()
        self.CodingHL_lossless = Model.CodingHL_lossless()
        self.CodingLH_lossless = Model.CodingLH_lossless()
        self.CodingHH_lossless = Model.CodingHH_lossless()

        init_scale = header.qp_shifts[header.model_index][header.block_qp_shift]

        if header.model_lambdas[header.model_index] < 0.10 + 0.001:
            # self.Transform_aiWave = Model.Transform_aiWave(init_scale, isAffine=True)
            # iWave use additive wavelet for all five lambda now
            self.Transform_aiWave = Model.Transform_aiWave(init_scale, isAffine=False)
        else:
            self.Transform_aiWave = Model.Transform_aiWave(init_scale, isAffine=False)
        self.CodingLL = Model.CodingLL()
        self.CodingHL = Model.CodingHL()
        self.CodingLH = Model.CodingLH()
        self.CodingHH = Model.CodingHH()
        self.Post = Model.Post()

        if header.second_step_filtering:
            assert (header.model_index > 4.5)  # Perceptual models have PostGAN
            self.PostGAN = Model.PostGAN()

    def decode(self):
        pass
