# Copyright (c) 2021-2022, InterDigital Communications, Inc
# All rights reserved.
import pdb
import random

import lpips
import numpy as np
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the disclaimer
# below) provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# * Neither the name of InterDigital Communications, Inc nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.

# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
# THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
# NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image
from pytorch_msssim import ms_ssim, MS_SSIM
from torchvision import transforms
from torchvision.models.detection import transform


def find_named_module(module, query):
    """Helper function to find a named module. Returns a `nn.Module` or `None`

    Args:
        module (nn.Module): the root module
        query (str): the module name to find

    Returns:
        nn.Module or None
    """

    return next((m for n, m in module.named_modules() if n == query), None)


def find_named_buffer(module, query):
    """Helper function to find a named buffer. Returns a `torch.Tensor` or `None`

    Args:
        module (nn.Module): the root module
        query (str): the buffer name to find

    Returns:
        torch.Tensor or None
    """
    return next((b for n, b in module.named_buffers() if n == query), None)


def _update_registered_buffer(
        module,
        buffer_name,
        state_dict_key,
        state_dict,
        policy="resize_if_empty",
        dtype=torch.int,
):
    new_size = state_dict[state_dict_key].size()
    registered_buf = find_named_buffer(module, buffer_name)

    if policy in ("resize_if_empty", "resize"):
        if registered_buf is None:
            raise RuntimeError(f'buffer "{buffer_name}" was not registered')

        if policy == "resize" or registered_buf.numel() == 0:
            registered_buf.resize_(new_size)

    elif policy == "register":
        if registered_buf is not None:
            raise RuntimeError(f'buffer "{buffer_name}" was already registered')

        module.register_buffer(buffer_name, torch.empty(new_size, dtype=dtype).fill_(0))

    else:
        raise ValueError(f'Invalid policy "{policy}"')


def update_registered_buffers(
        module,
        module_name,
        buffer_names,
        state_dict,
        policy="resize_if_empty",
        dtype=torch.int,
):
    """Update the registered buffers in a module according to the tensors sized
    in a state_dict.

    (There's no way in torch to directly load a buffer with a dynamic size)

    Args:
        module (nn.Module): the module
        module_name (str): module name in the state dict
        buffer_names (list(str)): list of the buffer names to resize in the module
        state_dict (dict): the state dict
        policy (str): Update policy, choose from
            ('resize_if_empty', 'resize', 'register')
        dtype (dtype): Type of buffer to be registered (when policy is 'register')
    """
    valid_buffer_names = [n for n, _ in module.named_buffers()]
    for buffer_name in buffer_names:
        if buffer_name not in valid_buffer_names:
            raise ValueError(f'Invalid buffer name "{buffer_name}"')

    for buffer_name in buffer_names:
        _update_registered_buffer(
            module,
            buffer_name,
            f"{module_name}.{buffer_name}",
            state_dict,
            policy,
            dtype,
        )


def conv(in_channels, out_channels, kernel_size=5, stride=2):
    return nn.Conv2d(
        in_channels,
        out_channels,
        kernel_size=kernel_size,
        stride=stride,
        padding=kernel_size // 2,
    )


def deconv(in_channels, out_channels, kernel_size=5, stride=2):
    return nn.ConvTranspose2d(
        in_channels,
        out_channels,
        kernel_size=kernel_size,
        stride=stride,
        output_padding=stride - 1,
        padding=kernel_size // 2,
    )


def quantize_ste(x):
    """Differentiable quantization via the Straight-Through-Estimator."""
    # STE (straight-through estimator) trick: x_hard - x_soft.detach() + x_soft
    return (torch.round(x) - x).detach() + x


def gaussian_kernel1d(
        kernel_size: int, sigma: float, device: torch.device, dtype: torch.dtype
):
    """1D Gaussian kernel."""
    khalf = (kernel_size - 1) / 2.0
    x = torch.linspace(-khalf, khalf, steps=kernel_size, dtype=dtype, device=device)
    pdf = torch.exp(-0.5 * (x / sigma).pow(2))
    return pdf / pdf.sum()


def gaussian_kernel2d(
        kernel_size: int, sigma: float, device: torch.device, dtype: torch.dtype
):
    """2D Gaussian kernel."""
    kernel = gaussian_kernel1d(kernel_size, sigma, device, dtype)
    return torch.mm(kernel[:, None], kernel[None, :])


def gaussian_blur(x, kernel=None, kernel_size=None, sigma=None):
    """Apply a 2D gaussian blur on a given image tensor."""
    if kernel is None:
        if kernel_size is None or sigma is None:
            raise RuntimeError("Missing kernel_size or sigma parameters")
        dtype = x.dtype if torch.is_floating_point(x) else torch.float32
        device = x.device
        kernel = gaussian_kernel2d(kernel_size, sigma, device, dtype)

    padding = kernel.size(0) // 2
    x = F.pad(x, (padding, padding, padding, padding), mode="replicate")
    x = torch.nn.functional.conv2d(
        x,
        kernel.expand(x.size(1), 1, kernel.size(0), kernel.size(1)),
        groups=x.size(1),
    )
    return x


def meshgrid2d(N: int, C: int, H: int, W: int, device: torch.device):
    """Create a 2D meshgrid for interpolation."""
    theta = torch.eye(2, 3, device=device).unsqueeze(0).expand(N, 2, 3)
    return F.affine_grid(theta, (N, C, H, W), align_corners=False)


class Space2Depth(nn.Module):
    """
    ref: https://github.com/huzi96/Coarse2Fine-PyTorch/blob/master/networks.py
    """

    def __init__(self, r=2):
        super().__init__()
        self.r = r

    def forward(self, x):
        r = self.r
        b, c, h, w = x.size()
        out_c = c * (r ** 2)
        out_h = h // r
        out_w = w // r
        x_view = x.view(b, c, out_h, r, out_w, r)
        x_prime = x_view.permute(0, 3, 5, 1, 2, 4).contiguous().view(b, out_c, out_h, out_w)
        return x_prime


class Depth2Space(nn.Module):
    def __init__(self, r=2):
        super().__init__()
        self.r = r

    def forward(self, x):
        r = self.r
        b, c, h, w = x.size()
        out_c = c // (r ** 2)
        out_h = h * r
        out_w = w * r
        x_view = x.view(b, r, r, out_c, h, w)
        x_prime = x_view.permute(0, 3, 4, 1, 5, 2).contiguous().view(b, out_c, out_h, out_w)
        return x_prime


def Demultiplexer(x):
    """
    See Supplementary Material: Figure 2.
    This operation can also implemented by slicing.
    """
    x_prime = Space2Depth(r=2)(x)

    _, C, _, _ = x_prime.shape
    anchor_index = tuple(range(C // 4, C * 3 // 4))
    non_anchor_index = tuple(range(0, C // 4)) + tuple(range(C * 3 // 4, C))

    anchor = x_prime[:, anchor_index, :, :]
    non_anchor = x_prime[:, non_anchor_index, :, :]

    return anchor, non_anchor


def Multiplexer(anchor, non_anchor):
    """
    The inverse opperation of Demultiplexer.
    This operation can also implemented by slicing.
    """
    _, C, _, _ = non_anchor.shape
    x_prime = torch.cat((non_anchor[:, : C // 2, :, :], anchor, non_anchor[:, C // 2:, :, :]), dim=1)
    return Depth2Space(r=2)(x_prime)


def Demultiplexerv2(x):
    """
    See Supplementary Material: Figure 2.
    This operation can also implemented by slicing.
    """
    x_prime = Space2Depth(r=2)(x)

    _, C, _, _ = x_prime.shape
    y1_index = tuple(range(0, C // 4))
    y2_index = tuple(range(C * 3 // 4, C))
    y3_index = tuple(range(C // 4, C // 2))
    y4_index = tuple(range(C // 2, C * 3 // 4))

    y1 = x_prime[:, y1_index, :, :]
    y2 = x_prime[:, y2_index, :, :]
    y3 = x_prime[:, y3_index, :, :]
    y4 = x_prime[:, y4_index, :, :]

    return y1, y2, y3, y4


def Multiplexerv2(y1, y2, y3, y4):
    """
    The inverse opperation of Demultiplexer.
    This operation can also implemented by slicing.
    """
    x_prime = torch.cat((y1, y3, y4, y2), dim=1)
    return Depth2Space(r=2)(x_prime)


def image2block(image: torch.Tensor, block_size: int, dim: int):
    b, c, h, w = image.shape
    block_num_h = int(np.floor(h / block_size))
    block_num_w = int(np.floor(w / block_size))
    block_num = block_num_h * block_num_w
    input_padded = torch.zeros(b, c, int(block_num_h * block_size), int(block_num_w * block_size)).cuda()
    input_padded[:, :, :h, :w] = image
    if dim == 1:
        output = torch.zeros((b, int(c * block_num), block_size, block_size)).cuda()
        for i in range(block_num_h):
            for j in range(block_num_w):
                cur_index = i * block_num_w + j
                current_block = image[:, :, int(i * block_size):int((i + 1) * block_size), int(j * block_size):int((j + 1) * block_size)]
                output[:, int(c * cur_index):int(c * (cur_index + 1)), :, :] = current_block
    elif dim == 0:
        output = torch.zeros((int(b * block_num), c, block_size, block_size)).cuda()
        for i in range(block_num_h):
            for j in range(block_num_w):
                cur_index = i * block_num_w + j
                current_block = image[:, :, int(i * block_size):int((i + 1) * block_size), int(j * block_size):int((j + 1) * block_size)]
                output[int(b * cur_index):int(b * (cur_index + 1)), :, :, :] = current_block

    else:
        raise NotImplementedError

    return output


def block2image(block: torch.Tensor, image_shape, dim=1):
    bb, cb, hb, wb = block.shape
    bi, ci, hi, wi = image_shape
    output = torch.zeros(image_shape).cuda()
    block_num_h = hi // hb
    block_num_w = wi // wb
    block_size = hb
    if dim == 0:
        for i in range(block_num_h):
            for j in range(block_num_w):
                cur_index = i * block_num_w + j
                output[:, :, int(i * block_size):int((i + 1) * block_size), int(j * block_size):int((j + 1) * block_size)] = block[int(bi * cur_index):int(bi * (cur_index + 1))][None, :, :, :]
    else:
        raise NotImplementedError

    return output


def save_tensor(tensor_input: torch.Tensor, output_path: str):
    tensor_input = torch.clamp(tensor_input, 0, 1)[0]
    tensor_input = torch.round(tensor_input * 255)
    tensor_input = tensor_input.permute((1, 2, 0)).detach().cpu()
    tensor_input = np.array(tensor_input, dtype='uint8')
    tensor_input = Image.fromarray(tensor_input)
    tensor_input.save(output_path)


def read_image(path: str) -> torch.Tensor:
    image = Image.open(path)
    # image = np.array(image)
    image = transforms.ToTensor()(image)
    return image


class VrUtils:
    def __init__(self, max_val, min_val):
        self.max_val = max_val
        self.min_val = min_val
        self.lambda_list = self.create_lambda_list(24)

    def create_lambda_list(self, num=20) -> list:
        step = (self.max_val - self.min_val) / num
        lambda_list = [self.max_val - step * i for i in range(num + 1)]
        return lambda_list

    def sample_lambda(self, batch_size) -> torch.Tensor:
        sample = random.sample(self.lambda_list, batch_size)
        sample = torch.Tensor(sample).view(batch_size, -1)
        return sample / self.max_val

    def sample_test_lambda(self, batch_size: int, num: int) -> torch.Tensor:
        lamb_list = self.create_lambda_list(num)
        lamb_list = torch.Tensor(lamb_list).view(1, -1)
        lamb_list = lamb_list.repeat(batch_size, 1)
        return lamb_list / self.max_val

    def d_loss(self, target: torch.Tensor, fake: torch.Tensor, lambda_list: torch.Tensor,
               loss_type: str) -> torch.Tensor:
        assert (loss_type == 'mse' or loss_type == 'ms-ssim')
        if loss_type == 'mse':
            mse = (target - fake) ** 2
            mse = torch.mean(mse, (1, 2, 3))
            mse = mse * (lambda_list[:, 0] * 255 ** 2) * self.max_val
            mse = torch.mean(mse)
            return mse
        else:
            mssim = 1. - ms_ssim(target, fake, data_range=1., size_average=False)
            mssim = mssim * lambda_list[:, 0] * self.max_val
            mssim = torch.mean(mssim)
            return mssim


class RateDistortionLoss(nn.Module):
    def __init__(self, lmbda=1e-2, metric='mse'):
        super().__init__()
        self.mse = nn.MSELoss()
        self.lmbda = lmbda
        self.metric = metric

    def forward(self, output, target):
        N, _, H, W = target.size()
        out = {}
        num_pixels = N * H * W

        out['bpp_loss'] = sum(
            (-torch.log2(likelihoods).sum() / num_pixels)
            for likelihoods in output['likelihoods'].values()
        )

        if self.metric == 'mse':
            out['mse_loss'] = self.mse(output['x_hat'], target)
            out["ms_ssim_loss"] = None
            out["loss"] = self.lmbda * 255 ** 2 * out["mse_loss"] + out["bpp_loss"]

        elif self.metric == 'ms-ssim':
            out["mse_loss"] = None
            out["ms_ssim_loss"] = 1 - MS_SSIM(output["x_hat"], target, data_range=1.0)
            out["loss"] = self.lmbda * out["ms_ssim_loss"] + out["bpp_loss"]

        return out


class WeightedRdLoss(RateDistortionLoss):
    def __init__(self, metric, vr_util: VrUtils):
        super(WeightedRdLoss, self).__init__(metric=metric)
        self.vr_util = vr_util
        self.lpips_loss = lpips.LPIPS().cuda()

    def forward(self, output, target, lambda_list):
        N, _, H, W = target.size()
        out = {}
        num_pixels = N * H * W

        out['bpp_loss'] = sum(
            (-torch.log2(likelihoods).sum() / num_pixels)
            for likelihoods in output['likelihoods'].values()
        )

        if self.metric == 'mse':
            out['mse_loss'] = self.vr_util.d_loss(output['x_hat'], target, lambda_list, self.metric)
            out["ms_ssim_loss"] = None
            out["loss"] = out["mse_loss"] + out["bpp_loss"]

        elif self.metric == 'ms-ssim':
            out["mse_loss"] = None
            out["ms_ssim_loss"] = self.vr_util.d_loss(output['x_hat'], target, lambda_list, self.metric)
            out["loss"] = out["ms_ssim_loss"] + out["bpp_loss"]
        elif self.metric == 'LPIPS':
            out["lpips_loss"] = self.lpips_loss(output['x_hat'], target).mean()
            out["loss"] = out["lpips_loss"]+((output['x_hat']-target)**2).mean()

        return out


def copy_lambda(lamb: torch.Tensor, block_num: int):
    output = torch.zeros((lamb.shape[0] * block_num, 1)).cuda()
    for i in range(lamb.shape[0]):
        output[i * block_num:(i + 1) * block_num, :] = lamb[i][None].repeat((block_num, 1))
    return output


if __name__ == '__main__':
    lamb = torch.Tensor([[1], [2], [3]])
    lamb_copy = copy_lambda(lamb, 4)
    print(lamb_copy)
