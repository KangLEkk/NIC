'''
    Contributors: Haojie Liu, Tong Chen, Chuanmin Jia, Yihang Chen
'''

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as f
from torch import Tensor
import AE
from Common.models.NIC_models.basic_module import ResBlock
from Common.models.NIC_models.gaussian_entropy_model import Distribution_for_entropy2
from typing import Any, Dict, Tuple
import torch.nn.functional as F


class Space2Depth(nn.Module):
    """
    ref: https://github.com/huzi96/Coarse2Fine-PyTorch/blob/master/networks.py
    """

    def __init__(self, r=2):
        super().__init__()
        self.r = r

    def forward(self, x):
        r = self.r
        b, c, h, w = x.size()
        out_c = c * (r ** 2)
        out_h = h // r
        out_w = w // r
        x_view = x.view(b, c, out_h, r, out_w, r)
        x_prime = x_view.permute(0, 3, 5, 1, 2, 4).contiguous().view(b, out_c, out_h, out_w)
        return x_prime


def Demultiplexerv2(x):
    """
    See Supplementary Material: Figure 2.
    This operation can also implemented by slicing.
    """
    x_prime = Space2Depth(r=2)(x)

    _, C, _, _ = x_prime.shape
    y1_index = tuple(range(0, C // 4))
    y2_index = tuple(range(C * 3 // 4, C))
    y3_index = tuple(range(C // 4, C // 2))
    y4_index = tuple(range(C // 2, C * 3 // 4))

    y1 = x_prime[:, y1_index, :, :]
    y2 = x_prime[:, y2_index, :, :]
    y3 = x_prime[:, y3_index, :, :]
    y4 = x_prime[:, y4_index, :, :]

    return y1, y2, y3, y4


def conv(in_channels, out_channels, kernel_size=5, stride=2):
    return nn.Conv2d(
        in_channels,
        out_channels,
        kernel_size=kernel_size,
        stride=stride,
        padding=kernel_size // 2,
    )


class MaskConv3d(nn.Conv3d):
    def __init__(self, mask_type, in_ch, out_ch, kernel_size, stride, padding):
        super(MaskConv3d, self).__init__(in_ch, out_ch,
                                         kernel_size, stride, padding, bias=True)

        self.mask_type = mask_type
        ch_out, ch_in, k, k, k = self.weight.size()
        mask = torch.zeros(ch_out, ch_in, k, k, k)
        central_id = k*k*k//2+1
        current_id = 1
        if mask_type == 'A':
            for i in range(k):
                for j in range(k):
                    for t in range(k):
                        if current_id < central_id:
                            mask[:, :, i, j, t] = 1
                        else:
                            mask[:, :, i, j, t] = 0
                        current_id = current_id + 1
        if mask_type == 'B':
            for i in range(k):
                for j in range(k):
                    for t in range(k):
                        if current_id <= central_id:
                            mask[:, :, i, j, t] = 1
                        else:
                            mask[:, :, i, j, t] = 0
                        current_id = current_id + 1
        if mask_type == 'C':
            for i in range(k):
                for j in range(k):
                    for t in range(k):
                        if current_id < central_id:
                            mask[:, :, i, j, t] = 0
                        else:
                            mask[:, :, i, j, t] = 1
                        current_id = current_id + 1
        self.register_buffer('mask', mask)

    def forward(self, x):
        self.weight.data *= self.mask
        return super(MaskConv3d, self).forward(x)


class Maskb_resblock(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size, stride, padding):
        super(Maskb_resblock, self).__init__()
        self.in_ch = int(in_channel)
        self.out_ch = int(out_channel)
        self.k = int(kernel_size)
        self.stride = int(stride)
        self.padding = int(padding)

        self.conv1 = MaskConv3d(
            'B', self.in_ch, self.out_ch, self.k, self.stride, self.padding)
        self.conv2 = MaskConv3d(
            'B', self.in_ch, self.out_ch, self.k, self.stride, self.padding)

    def forward(self, x):
        x1 = self.conv2(f.relu(self.conv1(x)))
        out = x + x1
        return out


class Resblock_3D(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size, stride, padding):
        super(Resblock_3D, self).__init__()
        self.in_ch = int(in_channel)
        self.out_ch = int(out_channel)
        self.k = int(kernel_size)
        self.stride = int(stride)
        self.padding = int(padding)

        self.conv1 = nn.Conv3d(self.in_ch, self.out_ch,
                               self.k, self.stride, self.padding)
        self.conv2 = nn.Conv3d(self.in_ch, self.out_ch,
                               self.k, self.stride, self.padding)

    def forward(self, x):
        x1 = self.conv2(f.relu(self.conv1(x)))
        out = x + x1
        return out


class P_Model(nn.Module):
    def __init__(self, M):
        super(P_Model, self).__init__()
        self.context_p = nn.Sequential(ResBlock(M, M, 3, 1, 1), ResBlock(M, M, 3, 1, 1), ResBlock(M, M, 3, 1, 1),
                                       nn.Conv2d(M, 2 * M, 3, 1, 1))

    def forward(self, x):

        x = self.context_p(x)
        return x


class Weighted_Gaussian(nn.Module):
    def __init__(self, M):
        super(Weighted_Gaussian, self).__init__()
        self.conv1 = MaskConv3d('A', 1, 24, 11, 1, 5)

        self.conv2 = nn.Sequential(nn.Conv3d(25, 48, 1, 1, 0), nn.ReLU(), nn.Conv3d(48, 96, 1, 1, 0), nn.ReLU(),
                                   nn.Conv3d(96, 9, 1, 1, 0))
        self.conv3 = nn.Conv2d(M * 2, M, 3, 1, 1)

        self.gaussin_entropy_func = Distribution_for_entropy2()

    def forward(self, x, hyper):
        x = torch.unsqueeze(x, dim=1)
        hyper = torch.unsqueeze(self.conv3(hyper), dim=1)
        x1 = self.conv1(x)
        output = self.conv2(torch.cat((x1, hyper), dim=1))
        p3 = self.gaussin_entropy_func(torch.squeeze(x, dim=1), output)
        return p3, output


class MultistageMaskedConv2d(nn.Conv2d):
    def __init__(self, *args: Any, mask_type: str = "A", **kwargs: Any):
        super().__init__(*args, **kwargs)

        self.register_buffer("mask", torch.zeros_like(self.weight.data))

        if mask_type == 'A':
            self.mask[:, :, 0::2, 0::2] = 1
        elif mask_type == 'B':
            self.mask[:, :, 0::2, 1::2] = 1
            self.mask[:, :, 1::2, 0::2] = 1
        elif mask_type == 'C':
            self.mask[:, :, :, :] = 1
            self.mask[:, :, 1:2, 1:2] = 0
        elif mask_type =='A/':
            self.mask[:, :, 0::2, 1::2] = 1
            self.mask[:, :, 1::2, 0::2] = 1
            self.mask[:, :, 1, 1]=1
        elif mask_type =='B/':
            self.mask[:, :, 0::2, 0::2] = 1
            self.mask[:, :, 1, 1]=1
        elif mask_type =='C/':
            self.mask[:, :, 1, 1]=1
        else:
            raise ValueError(f'Invalid "mask_type" value "{mask_type}"')

    def forward(self, x: Tensor) -> Tensor:
        # TODO: weight assigment is not supported by torchscript
        self.weight.data *= self.mask
        return super().forward(x)


class Multistage(nn.Module):
    def __init__(self, chs):
        super(Multistage, self).__init__()
        self.cc_transforms = nn.ModuleList(
            [
                nn.Sequential(
                    conv(in_channels=chs * 2, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs // 2, kernel_size=3, stride=1)
                ),
                nn.Sequential(
                    conv(in_channels=chs * 2 + chs // 2, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs, kernel_size=5, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs, out_channels=chs // 2, kernel_size=3, stride=1)
                )]
        )

        self.entropy_parameters = nn.ModuleList(
            [
                nn.Sequential(
                    conv(in_channels=chs * 3, out_channels=chs + chs // 3 * 4, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 4, out_channels=chs + chs // 3 * 2, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 2, out_channels=chs // 2 * 9, kernel_size=1, stride=1)
                ),
                nn.Sequential(
                    conv(in_channels=chs * 3, out_channels=chs + chs // 3 * 4, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 4, out_channels=chs + chs // 3 * 2, kernel_size=1, stride=1),
                    nn.GELU(),
                    conv(in_channels=chs + chs // 3 * 2, out_channels=chs // 2 * 9, kernel_size=1, stride=1)
                )
            ]
        )
        self.sc_transform_1 = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A'
            )
        ])
        
        self.sc_transform_1_plus = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A/'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='A/'
            )
        ])
        
        self.sc_transform_2 = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B'
            )
        ])
        
        self.sc_transform_2_plus=nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B/'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='B/'
            )
        ])
        
        self.sc_transform_3 = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C'
            )
        ])
        
        self.sc_transform_3_plus = nn.ModuleList([
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C/'
            ),
            MultistageMaskedConv2d(
                chs // 2,
                chs // 2,
                kernel_size=3, padding=1, stride=1, mask_type='C/'
            )
        ])

        self.conv3 = nn.Conv2d(chs * 2, chs, 3, 1, 1)
        self.gaussin_entropy_func = Distribution_for_entropy2()
        self.chs = chs
        self.unshuffle = nn.PixelUnshuffle(2)

    def single_slice_multistage_encode(self,
                                       x: torch.Tensor,
                                       support_slices: torch.Tensor,
                                       params: torch.Tensor,
                                       slice_index: int,
                                       x1_predict) -> Tensor:
        cc_params = self.cc_transforms[slice_index](support_slices)
        sc_params_1 = torch.zeros_like(cc_params)
        sc_params = sc_params_1
        _param_out_1 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_1 = _param_out_1.clone()
        param_out_1[:, :, 0::2, 1::2] = 0
        param_out_1[:, :, 1::2, :] = 0
        x_hat_1 = x.clone()
        x_hat_1[:, :, 0::2, 1::2] = 0
        x_hat_1[:, :, 1::2, :] = 0

        y_0 = x_hat_1.clone()

        _sc_params_2 = self.sc_transform_1[slice_index](y_0)

        _sc_params_2_plus = self.sc_transform_1_plus[slice_index](x1_predict)
        _sc_params_2 = _sc_params_2 + _sc_params_2_plus
        
        sc_params_2 = _sc_params_2.clone()
        sc_params_2[:, :, 0::2, :] = 0
        sc_params_2[:, :, 1::2, 0::2] = 0
        sc_params = sc_params_1 + sc_params_2

        _param_out_2 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_2 = _param_out_2.clone()
        param_out_2[:, :, 0::2, :] = 0
        param_out_2[:, :, 1::2, 0::2] = 0

        x_hat_2 = x.clone()
        x_hat_2[:, :, 0::2, :] = 0
        x_hat_2[:, :, 1::2, 0::2] = 0

        y_1 = (x_hat_1 + x_hat_2).clone()
        _sc_params_3 = self.sc_transform_2[slice_index](y_1)

        _sc_params_3_plus = self.sc_transform_2_plus[slice_index](x1_predict)
        _sc_params_3 = _sc_params_3+_sc_params_3_plus

        sc_params_3 = _sc_params_3.clone()
        sc_params_3[:, :, 0::2, 0::2] = 0
        sc_params_3[:, :, 1::2, :] = 0
        sc_params = sc_params_1 + sc_params_2 + sc_params_3
        _param_out_3 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_3 = _param_out_3.clone()
        param_out_3[:, :, 0::2, 0::2] = 0
        param_out_3[:, :, 1::2, :] = 0

        x_hat_3 = x.clone()
        x_hat_3[:, :, 0::2, 0::2] = 0
        x_hat_3[:, :, 1::2, :] = 0

        y_2 = (x_hat_1 + x_hat_2 + x_hat_3).clone()
        _sc_params_4 = self.sc_transform_3[slice_index](y_2)

        _sc_params_4_plus = self.sc_transform_3_plus[slice_index](x1_predict)
        _sc_params_4 = _sc_params_4 + _sc_params_4_plus

        sc_params_4 = _sc_params_4.clone()
        sc_params_4[:, :, 0::2, :] = 0
        sc_params_4[:, :, 1::2, 1::2] = 0
        sc_params = sc_params_1 + sc_params_2 + sc_params_3 + sc_params_4
        _param_out_4 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_4 = _param_out_4.clone()
        param_out_4[:, :, 0::2, :] = 0
        param_out_4[:, :, 1::2, 1::2] = 0

        x_hat_4 = x.clone()
        x_hat_4[:, :, 0::2, :] = 0
        x_hat_4[:, :, 1::2, 1::2] = 0

        param_out = (param_out_1 + param_out_2 + param_out_3 + param_out_4).clone()
        return param_out

    def entropy_decode(self, header, param_out, sample, stage_num):
        Min_Main, Max_Main = header.Min_Main, header.Max_Main
        precise = 16
        prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = [Demultiplexerv2(torch.chunk(param_out, 9, dim=1)[i].squeeze(1))[stage_num]
                                                                            for i in range(9)]

        probs = torch.stack([prob0, prob1, prob2], dim=-1)
        probs = F.softmax(probs, dim=-1)
        # process the scale value to positive non-zero
        b, c, h, w = mean0.shape
        sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1])).cuda()
        y_main_q_res = torch.zeros((b, c, h * 2, w * 2)).cuda()
        scale0 = torch.abs(scale0)
        scale1 = torch.abs(scale1)
        scale2 = torch.abs(scale2)
        scale0[scale0 < 1e-6] = 1e-6
        scale1[scale1 < 1e-6] = 1e-6
        scale2[scale2 < 1e-6] = 1e-6
        # 3 gaussian distributions
        m0 = torch.distributions.normal.Normal(mean0, scale0)
        m1 = torch.distributions.normal.Normal(mean1, scale1)
        m2 = torch.distributions.normal.Normal(mean2, scale2)
        lower = torch.zeros(1, c, h, w, Max_Main - Min_Main + 2)
        for i in range(sample.shape[4]):
            # print("CDF:", i)
            lower0 = m0.cdf(sample[:, :, :, :, i].cuda() - 0.5)
            lower1 = m1.cdf(sample[:, :, :, :, i].cuda() - 0.5)
            lower2 = m2.cdf(sample[:, :, :, :, i].cuda() - 0.5)
            lower[:, :, :, :, i] = probs[:, :, :, :, 0] * lower0 + \
                                   probs[:, :, :, :, 1] * lower1 + probs[:, :, :, :, 2] * lower2
        cdf_m = lower.data.cpu().numpy() * ((1 << precise) - (Max_Main - Min_Main + 1))  # [1, c, h, w ,Max-Min+1]
        cdf_m = cdf_m.astype(np.int) + \
                sample.cpu().numpy().astype(np.int) - Min_Main
        cdf_m = cdf_m.reshape((b * c * h * w, -1))
        _y_main_q_res = AE.decode_cdf_batch(cdf_m.tolist())
        _y_main_q_res = torch.Tensor(_y_main_q_res).cuda().reshape(b, c, h, w)
        y_main_q_res = torch.zeros((b, c, h * 2, w * 2)).cuda()
        if stage_num == 0:
            h_index = 0
            w_index = 0
        elif stage_num == 1:
            h_index = 1
            w_index = 1
        elif stage_num == 2:
            h_index = 0
            w_index = 1
        elif stage_num == 3:
            h_index = 1
            w_index = 0
        y_main_q_res[:, :, h_index::2, w_index::2] = _y_main_q_res
        return y_main_q_res

    def single_slice_multistage_decode(self,
                                       support_slices: torch.Tensor,
                                       params: torch.Tensor,
                                       slice_index: int,
                                       predicted_y_hat: torch.Tensor,
                                       header,
                                       sample,
                                       x1_predict) -> torch.Tensor:
        half_channel_num = predicted_y_hat.shape[1] // 2
        predicted_y_hat = predicted_y_hat[:, half_channel_num * slice_index:half_channel_num * (slice_index + 1), :, :]
        cc_params = self.cc_transforms[slice_index](support_slices)
        sc_params_1 = torch.zeros_like(cc_params)
        sc_params = sc_params_1
        param_out_1 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_1[:, :, 0::2, 1::2] = 0
        param_out_1[:, :, 1::2, :] = 0
        y_main_q_res_1 = self.entropy_decode(header=header, param_out=param_out_1, sample=sample, stage_num=0)
        y_0 = (y_main_q_res_1 + predicted_y_hat).clone()
        y_0[:, :, 0::2, 1::2] = 0
        y_0[:, :, 1::2, :] = 0
        
        sc_params_2 = self.sc_transform_1[slice_index](y_0)
        sc_params_2_plus = self.sc_transform_1_plus[slice_index](x1_predict)
        sc_params_2 = sc_params_2 + sc_params_2_plus
        
        sc_params_2[:, :, 0::2, :] = 0
        sc_params_2[:, :, 1::2, 0::2] = 0
        sc_params = sc_params_1 + sc_params_2

        param_out_2 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_2[:, :, 0::2, :] = 0
        param_out_2[:, :, 1::2, 0::2] = 0
        y_main_q_res_2 = self.entropy_decode(header=header, param_out=param_out_2, sample=sample, stage_num=1)
        y_1 = (y_main_q_res_1 + y_main_q_res_2 + predicted_y_hat).clone()
        y_1[:, :, 0::2, 1::2] = 0
        y_1[:, :, 1::2, 0::2] = 0
        
        sc_params_3 = self.sc_transform_2[slice_index](y_1)
        sc_params_3_plus = self.sc_transform_2_plus[slice_index](x1_predict)
        sc_params_3 = sc_params_3 + sc_params_3_plus
        
        sc_params_3[:, :, 0::2, 0::2] = 0
        sc_params_3[:, :, 1::2, :] = 0
        sc_params = sc_params_1 + sc_params_2 + sc_params_3
        param_out_3 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        param_out_3[:, :, 0::2, 0::2] = 0
        param_out_3[:, :, 1::2, :] = 0
        y_main_q_res_3 = self.entropy_decode(header=header, param_out=param_out_3, sample=sample, stage_num=2)
        y_2 = (y_main_q_res_1 + y_main_q_res_2 + y_main_q_res_3 + predicted_y_hat).clone()
        y_2[:, :, 1::2, 0::2] = 0
        
        sc_params_4 = self.sc_transform_3[slice_index](y_2)
        sc_params_4_plus = self.sc_transform_3_plus[slice_index](x1_predict)
        sc_params_4 = sc_params_4 + sc_params_4_plus
        
        sc_params_4[:, :, 0::2, :] = 0
        sc_params_4[:, :, 1::2, 1::2] = 0
        sc_params = sc_params_1 + sc_params_2 + sc_params_3 + sc_params_4
        param_out_4 = self.entropy_parameters[slice_index](
            torch.cat((params, sc_params, cc_params), dim=1)
        )
        y_main_q_res_4 = self.entropy_decode(header=header, param_out=param_out_4, sample=sample, stage_num=3)
        return y_main_q_res_1 + y_main_q_res_2 + y_main_q_res_3 + y_main_q_res_4 + predicted_y_hat

    def forward(self,
                xq1_res: torch.Tensor,
                xq1: torch.Tensor,
                hyper: torch.Tensor,
                x1_predict: torch.Tensor) -> Tuple[Tensor, Tensor]:
        channel_num = xq1.shape[1]
        x_half = [xq1[:, :channel_num // 2, :, :], xq1[:, channel_num // 2:, :, :]]
        param_out_1 = self.single_slice_multistage_encode(x=x_half[0],
                                                          support_slices=hyper,
                                                          params=hyper,
                                                          slice_index=0,
                                                          x1_predict=x1_predict[:, :channel_num // 2, :, :])
        prob_1_0, mean_1_0, scale_1_0, prob_1_1, mean_1_1, scale_1_1, prob_1_2, mean_1_2, scale_1_2 = \
            [torch.chunk(param_out_1, 9, dim=1)[i].squeeze(1) for i in range(9)]
        support_slices = torch.cat([hyper] + [x_half[0]], dim=1)
        param_out_2 = self.single_slice_multistage_encode(x=x_half[1],
                                                          support_slices=support_slices,
                                                          params=hyper,
                                                          slice_index=1,
                                                          x1_predict=x1_predict[:,channel_num // 2:, :, :])
        prob_2_0, mean_2_0, scale_2_0, prob_2_1, mean_2_1, scale_2_1, prob_2_2, mean_2_2, scale_2_2 = \
            [torch.chunk(param_out_2, 9, dim=1)[i].squeeze(1) for i in range(9)]
        context_out = torch.cat((prob_1_0, prob_2_0, mean_1_0, mean_2_0, scale_1_0, scale_2_0,
                                 prob_1_1, prob_2_1, mean_1_1, mean_2_1, scale_1_1, scale_2_1,
                                 prob_1_2, prob_2_2, mean_1_2, mean_2_2, scale_1_2, scale_2_2), dim=1)
        context_out = context_out.reshape(context_out.shape[0], 9, self.chs, context_out.shape[-2], context_out.shape[-1])
        p3 = self.gaussin_entropy_func(xq1_res, context_out)
        return p3, context_out

    def compress(self,
                 xq1_res: torch.Tensor,
                 xq1: torch.Tensor,
                 hyper: torch.Tensor,
                 x1_predict: torch.Tensor) -> Tuple[Tensor, Tensor]:
        channel_num = xq1.shape[1]
        x_half = [xq1[:, :channel_num // 2, :, :], xq1[:, channel_num // 2:, :, :]]
        param_out_1 = self.single_slice_multistage_encode(x=x_half[0],
                                                          support_slices=hyper,
                                                          params=hyper,
                                                          slice_index=0,
                                                          x1_predict=x1_predict[:,:channel_num // 2, :, :])
        prob_1_0, mean_1_0, scale_1_0, prob_1_1, mean_1_1, scale_1_1, prob_1_2, mean_1_2, scale_1_2 = \
            [torch.cat(Demultiplexerv2(torch.chunk(param_out_1, 9, dim=1)[i].squeeze(1)), dim=1) for i in range(9)]
        support_slices = torch.cat([hyper] + [x_half[0]], dim=1)
        param_out_2 = self.single_slice_multistage_encode(x=x_half[1],
                                                          support_slices=support_slices,
                                                          params=hyper,
                                                          slice_index=1,
                                                          x1_predict=x1_predict[:,channel_num // 2:, :, :])
        prob_2_0, mean_2_0, scale_2_0, prob_2_1, mean_2_1, scale_2_1, prob_2_2, mean_2_2, scale_2_2 = \
            [torch.cat(Demultiplexerv2(torch.chunk(param_out_2, 9, dim=1)[i].squeeze(1)), dim=1) for i in range(9)]
        context_out = torch.cat((prob_1_0, prob_2_0, mean_1_0, mean_2_0, scale_1_0, scale_2_0,
                                 prob_1_1, prob_2_1, mean_1_1, mean_2_1, scale_1_1, scale_2_1,
                                 prob_1_2, prob_2_2, mean_1_2, mean_2_2, scale_1_2, scale_2_2), dim=1)
        context_out = context_out.reshape(context_out.shape[0], 9, self.chs * 4, context_out.shape[-2], context_out.shape[-1])
        xq1_res = torch.cat((torch.cat(Demultiplexerv2(xq1_res[:, :channel_num // 2, :, :]), dim=1),
                             torch.cat(Demultiplexerv2(xq1_res[:, channel_num // 2:, :, :]), dim=1)), dim=1)
        p3 = self.gaussin_entropy_func(xq1_res, context_out)
        return p3, context_out

    def decompress(self, header, params, predicted_y_hat):
        sample = np.arange(header.Min_Main, header.Max_Main + 1 + 1)  # [Min_V - 0.5 , Max_V + 0.5]

        # sample = torch.FloatTensor(sample)
        # # if True:
        # #     sample = sample.cuda()
        channel_num = predicted_y_hat.shape[1]
        decode_half_1 = self.single_slice_multistage_decode(header=header, params=params, predicted_y_hat=predicted_y_hat, sample=sample,
                                                            support_slices=params,
                                                            slice_index=0,x1_predict=predicted_y_hat[:,:channel_num//2,:,:])
        support_slices = torch.cat((params, decode_half_1), dim=1)
        decode_half_2 = self.single_slice_multistage_decode(header=header, params=params, predicted_y_hat=predicted_y_hat, sample=sample,
                                                            support_slices=support_slices,
                                                            slice_index=1,x1_predict=predicted_y_hat[:,channel_num//2:,:,:])
        return torch.cat((decode_half_1, decode_half_2), dim=1)


class Weighted_Gaussian_res(nn.Module):
    def __init__(self, M):
        super(Weighted_Gaussian_res, self).__init__()
        self.conv1 = MaskConv3d('A', 1, 24, 11, 1, 5)

        self.conv2 = nn.Sequential(nn.Conv3d(25, 48, 1, 1, 0), nn.ReLU(), nn.Conv3d(48, 96, 1, 1, 0), nn.ReLU(),
                                   nn.Conv3d(96, 9, 1, 1, 0))
        self.conv3 = nn.Conv2d(M * 2, M, 3, 1, 1)

        self.gaussin_entropy_func = Distribution_for_entropy2()

    def forward(self, xq1_res, xq1, hyper):
        x = torch.unsqueeze(xq1, dim=1)
        hyper = torch.unsqueeze(self.conv3(hyper), dim=1)
        x1 = self.conv1(x)
        output = self.conv2(torch.cat((x1, hyper), dim=1))
        p3 = self.gaussin_entropy_func(xq1_res, output)
        return p3, output


class Weighted_Gaussian_res_2mask(nn.Module):
    def __init__(self, M):
        super(Weighted_Gaussian_res_2mask, self).__init__()
        self.conv1 = MaskConv3d('A', 1, 24, 11, 1, 5)

        self.conv2 = nn.Sequential(nn.Conv3d(25, 48, 1, 1, 0), nn.ReLU(), nn.Conv3d(48, 96, 1, 1, 0), nn.ReLU(),
                                   nn.Conv3d(96, 9, 1, 1, 0))
        self.conv3 = nn.Conv2d(M * 2, M, 3, 1, 1)

        self.gaussin_entropy_func = Distribution_for_entropy2()

        ## added
        self.conv4 = MaskConv3d('C', 1, 24, 11, 1, 5)

    def forward(self, xq1_res, xq1, hyper, x1_predict):
        x = torch.unsqueeze(xq1, dim=1)
        hyper = torch.unsqueeze(self.conv3(hyper), dim=1)
        x1 = self.conv1(x)

        ## mask2 for prediction
        x1_1 = self.conv4(torch.unsqueeze(x1_predict, dim=1))
        x1 = x1 + x1_1

        output = self.conv2(torch.cat((x1, hyper), dim=1))
        p3 = self.gaussin_entropy_func(xq1_res, output)
        return p3, output