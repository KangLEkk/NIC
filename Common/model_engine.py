import os
import struct
import numpy as np
import torch
import torch.nn as nn

from Common.utils.bee_utils.testfuncs import get_model_list
from Common.utils.syntaxparse.parse import header
from Common.utils.nic_utils.config import dict
from Common.models.NIC_models.acceleration_utils import unpack_bools
from Common.HighLevel_Function.Bin_Symbol import NIC_Bin2Symbol,BEE_Bin2Symbol,iWave_Bin2Symbol
from Common.HighLevel_Function.Bits_Bin import NIC_Bits2Bin,BEE_Bits2Bin,iWave_Bits2Bin
from Common.HighLevel_Function.InverseTrans import NIC_InverseTrans,BEE_InverseTrans,iWave_InverseTrans
from Common.HighLevel_Function.Preprocess import NIC_Preprocess,BEE_Preprocess,iWave_Preprocess
from Common.HighLevel_Function.Symbol_Bin import NIC_Symbol2Bin,BEE_Symbol2Bin
from Common.HighLevel_Function.Bin_Bits import NIC_Bin2Bits,BEE_Bin2Bits
from Common.HighLevel_Function.Postprocess import NIC_Postprocess,BEE_Postprocess,iWave_Postprocess
from Common.HighLevel_Function.ForwardTrans import NIC_ForwardTrans,BEE_ForwardTrans,iWave_ForwardTrans



class ModelEngine(nn.Module):
    def __init__(self):
        super().__init__()
        self.header = header()
        # Encoder Components
        self.preprocess = None
        self.trans = None
        self.symbol2bin = None
        self.bin2bits = None
        # Decoder Components
        self.bits2bin = None
        self.bin2symbol = None
        self.inverse_trans = None
        self.postprocess = None

    def parse_syntax(self, string):
        self.header.read_header_from_stream(string)

    def read_cfg(self, cfgfile, image, rate):
        self.header.read_json_cfg(cfgfile, image, rate)
        assert self.header.coding_mode_selection_syntax.synthesisTransformIdx != None

    def build(self, codectype):
        if codectype == "Enc":
            self.initialpreprocess()
            self.initialtrans()
            self.initialsymbol2bin()
            self.initialbin2bits()
            return
        elif codectype == "Dec":
            self.initialbits2bin()
            self.initialbin2symbol()
            self.initialinversetrans()
            self.initialpostprocess()
            return

    def to(self, device):
        for m in self.children():
            m.to(device)

    def encode(self, cfg, image, rate, ckptdir, binName, device):
        self.read_cfg(cfg, image, rate)
        self.build("Enc")
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            model_list = get_model_list(ckptdir)
            self.modelload("Enc", model_list[self.header.picture.picture_header.quality], device)
            self.to(torch.device(device))
            self.eval()
            with torch.no_grad():
                input_variable = self.preprocess.encode(image, self.header, device)
                forward_trans_varlist = self.forward_trans.encode(input_variable, self.header, device)
                symbol2bin_varlist = self.symbol2bin.encode(forward_trans_varlist, self.header, device)
                self.bin2bits.encode(symbol2bin_varlist, self.header, binName)

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 1:
            model_path = self.header.picture.picture_header.model_path
            self.modelload("Enc", model_path, device)
            self.forward_trans.encode(image, self.header.picture.picture_header)

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.encode_block(image, ckptdir, binName, device)

    def encode_block(self, image, model_list, binName, device):
        img_class_path = os.path.dirname(os.path.dirname(binName))  ## bin上上级目录（默认bin路径：classC_2k/lambda_0018/Enc/a.bin）  ## 编码时保存的重见图片路径
        img_name = binName.split('/')[-1].split(".")[0]
        Rec_dir = os.path.join(img_class_path, f'{img_name}.png')
        print("img_class:", Rec_dir)

        picture = self.header.picture
        header = picture.picture_header
        # block_header = self.header.picture.block_header
        self.modelload("Enc", model_list, device)
        self.to('cuda')
        self.eval()  ## ad
        f = open(binName, 'wb')
        f = self.header.coding_mode_selection_syntax.write_header_to_stream(f)
        f, source_img, img_block_list = self.preprocess.encode(image, f, picture, device)
        # if use postprocessing, need to reconstruct image at encoder side
        if header.USE_POSTPROCESSING:
            out_img = np.zeros([header.H, header.W, header.C]) # recon image
            picture.block_header.H_offset = 0
            picture.block_header.W_offset = 0

        ######################### Padding Image #########################
        Block_Idx = 0
        for img in img_block_list:  # Traverse CTUs
            picture.block_header.block_H = img.shape[0]
            picture.block_header.block_W = img.shape[1]
            tile = 64.
            block_H_PAD = int(tile * np.ceil(picture.block_header.block_H / tile))
            block_W_PAD = int(tile * np.ceil(picture.block_header.block_W / tile))
            im = np.zeros([block_H_PAD, block_W_PAD, 3], dtype='float32')
            im[:picture.block_header.block_H, :picture.block_header.block_W, :] = img[:, :, :3]
            im = torch.FloatTensor(im)
            im = im.permute(2, 0, 1).contiguous()
            im = im.view(1, header.C, block_H_PAD, block_W_PAD)
            if header.GPU:
                im = im.cuda()
                if header.USE_VR_MODEL:
                    header.lambda_rd = header.lambda_rd.cuda()
            # print('====> Encoding Image:', im_dir, "%dx%d" % (block_H, block_W), 'to', out_dir,
            #     " Block Idx: %d" % (Block_Idx))
            Block_Idx += 1
            # begin processing CTU
            im_block_list = []
            im_block_loc_list = []
            im_block_list.append(im)  # list size = 1
            im_block_loc_list.append([0, 0, block_H_PAD, block_W_PAD])
            for im_block_loc, im_block in zip(im_block_loc_list, im_block_list):
                im_block = self.preprocess.process_block(im_block, header)
            forward_trans_varlist = self.forward_trans.encode(im_block, header)
            symbol2bin_varlist = self.symbol2bin.encode(forward_trans_varlist, out_img, picture, device)
            self.bin2bits.encode(symbol2bin_varlist, header, f, Block_Idx)
        self.postprocess.encode(source_img, out_img, f, header, Rec_dir)  ## 编码时也重建图片，路径Rec_dir

    def decode(self, string, recon_path, ckptdir, device):
        self.parse_syntax(string)
        self.build("Dec")
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            model_list = get_model_list(ckptdir)
            self.modelload("Dec", model_list[self.header.picture.picture_header.quality], device)
            self.to(torch.device(device))
            self.eval()
            bits2bin_varlist = self.bits2bin.decode(self.header, device)
            quant_latent = self.bin2symbol.decode(bits2bin_varlist, self.header, device)
            imArray = self.inverse_trans.decode(quant_latent, self.header)
            self.postprocess.decode(imArray, self.header, recon_path)
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 1:
            self.modelload("Dec", ckptdir, device)
            img_array = self.inverse_trans.decode(self.header, recon_path)
            if self.header.picture.picture_header.isLossless == 0:
                self.postprocess.decode(img_array, self.header, recon_path)

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.decode_block(recon_path, ckptdir, device)
        return

    def decode_block(self, recon_path, ckptdir, device):
        self.modelload("Dec", ckptdir, device)
        header = self.header.picture.picture_header
        block_header = self.header.picture.block_header
        Block_Idx = -1
        imArray = np.zeros([self.header.picture.picture_header.H, self.header.picture.picture_header.W, 3])
        H_offset, W_offset = 0, 0
        for i_block in range(header.Block_Num_in_Height):
            for j_block in range(header.Block_Num_in_Width):
                Block_Idx += 1
                if header.USE_ACCELERATION:
                    if Block_Idx == 0:
                        EC_this_block = unpack_bools(self.bits2bin.c_main, self.header.picture.f)
                    else:
                        b_len = struct.calcsize('1?')
                        bits = self.header.picture.f.read(b_len)
                        b = struct.unpack('1?', bits)
                        if b == 1:
                            EC_this_block = EC_last_block
                        else:
                            EC_this_block = unpack_bools(self.bits2bin.c_main, self.header.picture.f)
                    EC_last_block = EC_this_block
                block_H = header.block_height
                block_W = header.block_width
                if header.USE_ACCELERATION:
                    block_header.EC_this_block = EC_this_block
                else:
                    block_header.EC_this_block = [1] * self.bits2bin.c_main
                if i_block == header.Block_Num_in_Height - 1:
                    block_H = header.H - (header.Block_Num_in_Height - 1) * block_H
                if j_block == header.Block_Num_in_Width - 1:
                    block_W = header.W - (header.Block_Num_in_Width - 1) * block_W
                print('==================> Decoding Block:', "(%d, %d)" % (i_block, j_block),
                      "[%d, %d]" % (block_H, block_W))
                tile = 64.
                block_H_PAD = int(tile * np.ceil(block_H / tile))
                block_W_PAD = int(tile * np.ceil(block_W / tile))
                block_loc_list = []
                block_loc_list.append([0, 0, block_H_PAD, block_W_PAD])
                for block_loc in block_loc_list:
                    # block_loc -> [vertical_location, horizontal_location, block_height, block_width]
                    print('==================> Decoding sub_block:',
                          "(%d, %d, %d, %d)" % (block_loc[0], block_loc[1], block_loc[2], block_loc[3]))
                    y_hyper_q, block_header, bin_dir_path = self.bits2bin.decode(self.header.picture.f, block_loc, header, block_header)
                    # quant_latent = self.bin2symbol.decode(y_hyper_q, header, block_header)
                    ## 改，传入输入的bin文件路径。self.header.picture.f为bin文件名
                    quant_latent = self.bin2symbol.decode(y_hyper_q, header, block_header, bin_dir_path)
                    imArray_block = self.inverse_trans.decode(quant_latent, header, block_header)
                    imArray[H_offset: H_offset + block_H, W_offset: W_offset + block_W, :] = imArray_block[:block_H, :block_W, :]
                del block_loc_list
                W_offset += block_W
                if W_offset >= header.W:
                    W_offset = 0
                    H_offset += block_H
        self.postprocess.decode(imArray, self.header.picture.f, header, recon_path)
        return

    def modelload(self, codectype, ckpt, device):
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            checkpoint = torch.load(ckpt, map_location=torch.device(device))
            if codectype == "Enc":
                self.bin2bits.load_state_dict(checkpoint, strict=False)
                self.symbol2bin.load_state_dict(checkpoint, strict=False)
                self.forward_trans.load_state_dict(checkpoint, strict=False)
            if codectype == "Dec":
                self.bits2bin.load_state_dict(checkpoint, strict=False)
                self.bin2symbol.load_state_dict(checkpoint, strict=False)
                self.inverse_trans.load_state_dict(checkpoint, strict=False)
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 1:
            pic_header = self.header.picture.picture_header
            if pic_header.isLossless:
                qp_shift = 0
                model_qp = 27
                init_scale = pic_header.qp_shifts[model_qp][qp_shift]
                checkpoint = torch.load(ckpt + '/' + str(pic_header.model_lambdas[model_qp]) + '_lossless.pth')
                all_part_dict = checkpoint['state_dict']

                models_dict = {}
                models_dict['transform'] = self.bits2bin.Transform_lossless
                models_dict['entropy_LL'] = self.bits2bin.CodingLL_lossless
                models_dict['entropy_HL'] = self.bits2bin.CodingHL_lossless
                models_dict['entropy_LH'] = self.bits2bin.CodingLH_lossless
                models_dict['entropy_HH'] = self.bits2bin.CodingHH_lossless
                models_dict_update = {}
                for key, model in models_dict.items():
                    myparams_dict = model.state_dict()
                    part_dict = {k: v for k, v in all_part_dict.items() if k in myparams_dict}
                    myparams_dict.update(part_dict)
                    model.load_state_dict(myparams_dict)
                    if torch.cuda.is_available():
                        model = model.cuda()
                        model.eval()
                    models_dict_update[key] = model
                models_dict.update(models_dict_update)

                if codectype == "Dec":
                    self.inverse_trans.models_dict = models_dict
                elif codectype == "Enc":
                    self.forward_trans.models_dict = models_dict
            else:
                model_qp = pic_header.model_index

                if model_qp > 12.5:  # 13-->26 Perceptual models
                    checkpoint = torch.load(ckpt + '/' + str(pic_header.model_lambdas[model_qp]) + '_percep.pth')
                else:  # 0-->12 MSE models
                    checkpoint = torch.load(ckpt + '/' + str(pic_header.model_lambdas[model_qp]) + '_mse.pth')

                all_part_dict = checkpoint['state_dict']

                models_dict = {}

                models_dict['transform'] = self.bits2bin.Transform_aiWave
                models_dict['entropy_LL'] = self.bits2bin.CodingLL
                models_dict['entropy_HL'] = self.bits2bin.CodingHL
                models_dict['entropy_LH'] = self.bits2bin.CodingLH
                models_dict['entropy_HH'] = self.bits2bin.CodingHH
                models_dict['post'] = self.bits2bin.Post
                if pic_header.second_step_filtering:
                    models_dict['postGAN'] = self.bits2bin.PostGAN

                models_dict_update = {}
                for key, model in models_dict.items():
                    myparams_dict = model.state_dict()
                    part_dict = {k: v for k, v in all_part_dict.items() if k in myparams_dict}
                    myparams_dict.update(part_dict)
                    model.load_state_dict(myparams_dict)
                    if torch.cuda.is_available():
                        model = model.cuda()
                        model.eval()
                    models_dict_update[key] = model
                models_dict.update(models_dict_update)

                if codectype == "Dec":
                    self.inverse_trans.models_dict = models_dict
                    self.postprocess.model_post = models_dict['post']
                    if pic_header.second_step_filtering:
                        self.postprocess.model_postGAN = models_dict['postGAN']
                elif codectype == "Enc":
                    self.forward_trans.models_dict = models_dict
                    self.postprocess.model_post = models_dict['post']
                    if pic_header.second_step_filtering:
                        self.postprocess.model_postGAN = models_dict['postGAN']

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            ######################### Load Model #########################
            # index - [0-15]
            USE_VR_MODEL = dict['USE_VR_MODEL']
            if USE_VR_MODEL:
                models = ["mse_VR_low", "mse_VR_middle", "mse_VR_high", "msssim_VR_low", "msssim_VR_middle", "msssim_VR_high"]
                max_lambdas = [6, 44, 296, 0.08, 0.96, 7.68]
            else:
                models = ["mse10", "mse42", "mse160", "mse618", "mse2950",
                          "msssim140", "msssim650", "msssim2700", "msssim11537", "msssim44000"]
            checkpoint = torch.load(os.path.join(ckpt, models[self.header.picture.picture_header.model_index] + r'.pkl'), map_location='cpu')
            checkpoint_context = torch.load(os.path.join(ckpt, models[self.header.picture.picture_header.model_index] + r'p.pkl'), map_location='cpu')
            if codectype == "Dec":
                self.bits2bin.load_state_dict(torch.load(
                    os.path.join(ckpt, models[self.header.picture.picture_header.model_index] + r'.pkl'), map_location='cpu'), strict=False)
                self.bin2symbol.load_state_dict(torch.load(
                    os.path.join(ckpt, models[self.header.picture.picture_header.model_index] + r'.pkl'), map_location='cpu'), strict=False)
                self.inverse_trans.load_state_dict(torch.load(
                    os.path.join(ckpt, models[self.header.picture.picture_header.model_index] + r'.pkl'), map_location='cpu'), strict=False)
                self.bin2symbol.context.load_state_dict(torch.load(
                    os.path.join(ckpt, models[self.header.picture.picture_header.model_index] + r'p.pkl'), map_location='cpu'))
                if dict['GPU']:
                    self.bits2bin = self.bits2bin.cuda()
                    self.bin2symbol = self.bin2symbol.cuda()
                    self.inverse_trans = self.inverse_trans.cuda()
                    self.bin2symbol.context = self.bin2symbol.context.cuda()
            if codectype == "Enc":
                ## added
                # self.preprocess = model_load(self.preprocess, checkpoint)

                self.preprocess.image_comp.load_state_dict(checkpoint, strict=False)  ## 改
                self.preprocess.context.load_state_dict(checkpoint_context)
                self.bin2bits.load_state_dict(checkpoint, strict=False)
                self.symbol2bin.load_state_dict(checkpoint, strict=False)
                self.forward_trans.load_state_dict(checkpoint, strict=False)
                self.symbol2bin.context.load_state_dict(checkpoint_context)

    def initialpreprocess(self):
        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 0:
            self.preprocess = BEE_Preprocess()

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 1:
            self.preprocess = iWave_Preprocess()

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 2:
            self.preprocess = NIC_Preprocess(self.header.picture.picture_header)
        return

    def initialtrans(self):
        assert self.header.coding_mode_selection_syntax.synthesisTransformIdx != None
        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 0:
            self.forward_trans = BEE_ForwardTrans()
            self.forward_trans.update(self.header)
            return
        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 1:
            self.bits2bin = iWave_Bits2Bin(self.header)
            self.postprocess = iWave_Postprocess()
            self.forward_trans = iWave_ForwardTrans(self.header.picture.picture_header)

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 2:
            self.forward_trans = NIC_ForwardTrans(self.header.picture.picture_header)

    def initialsymbol2bin(self):
        assert self.header.coding_mode_selection_syntax.bin2symbolIdx != None
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            self.symbol2bin = BEE_Symbol2Bin()
            self.symbol2bin.update(self.header)
            return

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 1:
            return

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.symbol2bin = NIC_Symbol2Bin(self.header.picture.picture_header)

    def initialbin2bits(self):
        assert self.header.coding_mode_selection_syntax.bin2symbolIdx != None
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            self.bin2bits = BEE_Bin2Bits()
            self.bin2bits.update()
            return

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.bin2bits = NIC_Bin2Bits(self.header.picture.picture_header)
            self.postprocess = NIC_Postprocess()
            return

    def initialbits2bin(self):
        assert self.header.coding_mode_selection_syntax.bin2symbolIdx != None
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            self.bits2bin = BEE_Bits2Bin()
            self.bits2bin.update(self.header)
            return

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 1:
            self.bits2bin = iWave_Bits2Bin(self.header)
            return

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.bits2bin = NIC_Bits2Bin(self.header.picture.picture_header)
            return

    def initialbin2symbol(self):
        assert self.header.coding_mode_selection_syntax.bin2symbolIdx != None
        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 0:
            self.bin2symbol = BEE_Bin2Symbol()
            self.bin2symbol.update(self.header)
            return

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 1:
            self.bin2symbol = iWave_Bin2Symbol()

        if self.header.coding_mode_selection_syntax.bin2symbolIdx == 2:
            self.bin2symbol = NIC_Bin2Symbol(self.header.picture.picture_header)

    def initialinversetrans(self):
        assert self.header.coding_mode_selection_syntax.synthesisTransformIdx != None
        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 0:
            self.inverse_trans = BEE_InverseTrans()
            self.inverse_trans.update(self.header)
            return

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 1:
            self.inverse_trans = iWave_InverseTrans(self.header)

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 2:
            self.inverse_trans = NIC_InverseTrans(self.header.picture.picture_header)

    def initialpostprocess(self):
        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 0:
            self.postprocess = BEE_Postprocess()
            return

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 1:
            self.postprocess = iWave_Postprocess()
            return

        if self.header.coding_mode_selection_syntax.synthesisTransformIdx == 2:
            self.postprocess = NIC_Postprocess()
